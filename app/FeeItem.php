<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeItem extends Model
{
    //

    protected $guarded = [];
    protected $table = 'tblfeeitems';
    public $timestamps = false;


    public function getRouteKeyName()
    {
        return 'FeeItemID';
    }

    public function getKeyName()
    {
        return 'FeeItemID';
    }
}
