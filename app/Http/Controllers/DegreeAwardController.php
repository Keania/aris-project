<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use Illuminate\Database\QueryException;
use App\DegreeAward;
use App\DegreeType;

class DegreeAwardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $degree_awards = DegreeAward::all();
        $degree_types = DegreeType::all();
        return view('setup.degree_award', ['degree_awards' => $degree_awards,'degree_types'=>$degree_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'degreetype_id' => 'required|numeric',
            'degreeaward'=>'required',
            'degreeawardcode'=>'required'
        ]);

        $degreeaward = $request->degreeaward;
      

        try {

            if (DegreeAward::where('DegreeAward', $degreeaward)->where('DegreeAwardCode',$request->degreeawardcode)->where('DegreeTypeID',$request->degreetype_id)->exists()) {
              
                throw new Exception('Degree Award already exists in database');
            }
          
            DegreeAward::create([
                'DegreeAward' => $degreeaward,
                'DegreeAwardCode'=>$request->degreeawardcode,
                'DegreeTypeID'=>$request->degreetype_id
            ]);
           
        } catch (QueryException | Exception $e) {
           
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Degree Award successfully added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $request->validate([
            'degreeaward_id' => 'required',
            'degreetype_id' => 'required',
            'degreeaward'=>'required',
            'degreeawardcode'=>'required'
        ]);


        try {

            $degreeaward = DegreeAward::where('DegreeAwardID', $request->degreeaward_id)->first();

           
            if (DegreeAward::where('DegreeAward', $request->degreeaward)->where('DegreeAwardCode',$request->degreeawardcode)->exists()) {
                throw new Exception('The new Degree Award name already exists in the database');
            }


            $degreeaward->update([
                'DegreeAward'=>$request->degreeaward,
                'DegreeAwardCode'=>$request->degreeawardcode,
                'DegreeTypeID'=>$request->degreetype_id
            ]);
        } catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Degree Award successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $request->validate([
            'degreeaward_id' => 'required',
        ]);

        try {
            $degreeaward = DegreeAward::where('DegreeAwardID', $request->degreeaward_id)->first();
        
            $degreeaward->delete();
        } catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Degree Award successfully deleted']);
    }
}
