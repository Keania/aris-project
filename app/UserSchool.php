<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSchool extends Model
{
    //

    protected $table = 'tblusers_schools';
    protected $guarded = [];
    public $timestamps = false;
}
