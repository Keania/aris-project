<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\LGA;
use Faker\Generator as Faker;
use App\State;

$factory->define(LGA::class, function (Faker $faker) {
    $state_id = State::all()->pluck('StateID');
    return [
        //
        'StateID'=>$faker->randomElement($state_id),
        'LGA'=>$faker->sentence(2)
    ];
});
