<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\FacultyColour;
use Exception;

class FacultyColourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'colour_name' => 'required',
        ]);

        $colour_name = $request->colour_name;
        try {
            if (FacultyColour::where('ColourName', $colour_name)->exists()) {

                throw new Exception('Faculty Colour already exists in database');
            }
            FacultyColour::create([
                'ColourName'=>$colour_name,
            ]);
           
        } catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Faculty Colour successfully added']);
    
}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
    //
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{
    //
}

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request)
{
    //
    $request->validate([
        'colour_name'=>'required',
        'colour_id'=>'required'
    ]);

    
    try {

        $colour = FacultyColour::where('ColourID',$request->colour_id)->first();

        if(FacultyColour::where('ColourName',$request->colour_name)->exists()){
            throw new Exception('The new colour name already exists in the database');
        }

        if($colour->appliedFaculty->isNotEmpty()){

            throw new Exception('The colour has been applied as a theme and cannot be changed');
        }

        $colour->update(['ColourName'=>$request->colour_name]);

    }catch (QueryException | Exception $e) {
        return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
    }
    return redirect()->back()->with(['success' => 'Colour successfully updated']);
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function destroy(Request $request)
{
    //
    $request->validate([
        'colour_id'
    ]);
    try {

        $colour = FacultyColour::where('ColourID',$request->colour_id)->first();

        if($colour->appliedFaculty->isNotEmpty()){
            
            throw new Exception('The colour has been applied as a theme and cannot be deleted');
        }

        $colour->delete();

    }catch (QueryException | Exception $e) {
        return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
    }
    return redirect()->back()->with(['success' => 'Colour successfully deleted']);
}
}
