@extends('app')
@section('page.title')
LGA & State Set Up
@endsection

@section('page.content')
<div class="section__content">

    <div id="lga">
        <h3><i>Set up</i>Local Government Area</h3>

        <div id="add__lga" class="setup__divs">
            <h5>New LGA definition</h5>
            <form method="POST" action="{{route('save.lga')}}">
                @csrf

                <div class="form-group col-md-6 row">

                    <label class="form-label col-md-4">Select State</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('state_id') ? 'invalid' : ''}}" name="state_id">
                            @if(count($states) > 0)
                            <option value="null">--Select State--</option>
                            @foreach($states as $state)
                            <option value="{{$state->StateID}}">{{$state->State}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                       
                        @if($errors->has('state_id'))
                        <span class="errors">{{$errors->first('state_id')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Lga</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{$errors->has('lga') ? 'invalid' : ''}}" name="lga">
                        @if($errors->has('lga'))
                        <span class="errors">{{$errors->first('lga')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Add" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#edit__lga" value="Edit" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#delete__lga" value="Delete" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="edit__lga" class="collapse setup__divs">

            <h5>LGA Modification</h5>
            <form method="POST" action="{{route('update.lga')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select State</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('state_id') ? 'invalid' : ''}} " name="state_id" onchange="getlgas(event)">

                            @if(count($states) > 0)
                            <option value="null">--Select State--</option>
                            @foreach($states as $state)
                            <option value="{{$state->StateID}}">{{$state->State}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                        @if($errors->has('state_id'))
                        <span class="errors">{{$errors->first('state_id')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select lga</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('lga_id') ? 'invalid' : ''}} " name="lga_id" onchange="selectlga(event)" id="lgas">
                            <option>None</option>
                            @if($errors->has('lga_id'))
                            <span class="errors">{{$errors->first('lga_id')}}</span>
                            @endif
                        </select>
                    </div>

                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">lga</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control  {{$errors->has('lga') ? 'invalid' : ''}} " name="lga" id="editLGAInput">
                        @if($errors->has('lga'))
                        <span class="errors">{{$errors->first('lga')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Save" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="delete__lga" class="collapse setup__divs">
            <h5>LGA deletion</h5>
            <form method="POST" action="{{route('delete.lga')}}">
                @csrf
                <div class="form-group col-md-6">
                    <label class="form-label">Select lga</label>
                    <select class="form-control" name="lga_id">
                        @if(count($lgas) > 0)
                        @foreach($lgas as $lga)
                        <option value="{{$lga->LGAID}}">{{$lga->LGA}}</option>
                        @endforeach
                        @else
                        <option>No lga available</option>
                        @endif
                    </select>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Delete" class="btn btn-primary">

                </div>
            </form>
        </div>
    </div>

    <div id="states">
        <h3><i>Set up</i>State</h3>

        <div id="add__state" class="setup__divs">
            <h5>State definition</h5>
            <form method="POST" action="{{route('save.state')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">State</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{$errors->has('state') ? 'invalid' : ''}}" name="state">
                        @if($errors->has('state'))
                        <span class="errors">{{$errors->first('state')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Add" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#edit__state" value="Edit" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#delete__state" value="Delete" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="edit__state" class="collapse setup__divs">

            <h5>State Modification</h5>
            <form method="POST" action="{{route('update.state')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select State</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('state_id') ? 'invalid' : ''}} " name="state_id" onchange="getState(event)" id="stateList">

                            @if(count($states) > 0)
                            <option value="null">--Select State--</option>
                            @foreach($states as $state)
                            <option value="{{$state->StateID}}">{{$state->State}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                        @if($errors->has('state_id'))
                        <span class="errors">{{$errors->first('state_id')}}</span>
                        @endif
                    </div>

                </div>


                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">State</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control  {{$errors->has('state') ? 'invalid' : ''}} " name="state" id="editStateNameInput">
                        @if($errors->has('state'))
                        <span class="errors">{{$errors->first('state')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Save" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="delete__state" class="collapse setup__divs">
            <h5>State deletion</h5>
            <form method="POST" action="{{route('delete.state')}}">
                @csrf
                <div class="form-group col-md-6">
                    <label class="form-label">Select State</label>
                    <select class="form-control" name="state_id">
                        @if(count($states) > 0)
                        @foreach($states as $state)
                        <option value="{{$state->StateID}}">{{$state->State}}</option>
                        @endforeach
                        @else
                        <option>No colour available</option>
                        @endif
                    </select>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Delete" class="btn btn-primary">

                </div>
            </form>
        </div>
    </div>


</div>
@endsection
@section('page.script')
<script>
    var getlgas = function(evt) {

        let select = evt.target.value;
        if (isNaN(select)) {

            return false;
        }
        $.getJSON('/api/lga/state/' + select, function(response) {


        }).done(function(response) {
            var selecter = document.getElementById('lgas');
            selecter.innerHTML = "";
            response.data.forEach(function(lga) {
                console.log(lga)
                option = document.createElement('option')
                option.setAttribute('value', lga.LGAID);
                option.innerHTML = lga.LGA;
                selecter.appendChild(option);
            })

        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        });
    }


    var selectlga = function(evt) {

        let select = evt.target.value;
        if (isNaN(select)) {
            return false;
        }
        $.getJSON('/api/lga/' + select, function(response) {}).done(function(response) {
            document.getElementById('editLGAInput').value = response.data.LGA
            
        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        })

    }

    var getState = function(evt) {

        let select = evt.target.value;
        console.log(select)
        if (isNaN(select)) {
            return false;
        }
        $.getJSON('/api/state/' + select, function(response) {}).done(function(response) {
            document.getElementById('editStateNameInput').value = response.data.State
           
        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        })

    }
</script>
@endsection