<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    //
    protected $guarded = [];
    protected $table = 'tblschools';
    public $timestamps = false;
}
