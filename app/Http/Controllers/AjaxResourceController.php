<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PredataCategory;
use App\PreData;
use App\FacultyColour;
use App\State;
use App\LGA;
use App\OlevelSubject;
use App\DegreeType;
use App\ProgramType;
use App\DegreeAward;
use App\FeeItem;

class AjaxResourceController extends Controller
{
    //

    public function getCategoryPredatas(Request $request,PredataCategory $category)
    {
       
        return response()->json(['data'=>$category->predatas]);
    }


    public function getPredata(Request $request,Predata $predata)
    {
        return response()->json(['data'=>$predata]);
    }

    public function getColour(FacultyColour $facultycolour)
    {
        return response()->json(['data'=>$facultycolour]);
    }

    public function getStatelgas(State $state){

        return response()->json(['data'=>$state->lgas]);
    }

    public function getState(State $state)
    {
        return response()->json(['data'=>$state]);
    }

    public function getLGA(LGA $lga)
    {
        return response()->json(['data'=>$lga]);
    }

    public function getOlevelSubject(OlevelSubject $olevelsubject)
    {
        return response()->json(['data'=>$olevelsubject]);
    }

    public function getDegreeType(DegreeType $degreetype)
    {
        return response()->json(['data'=>$degreetype]);
    }

    public function getProgramType(ProgramType $programtype)
    {
        return response()->json(['data'=>$programtype]);
    }

    public function getDegreeAward(DegreeAward $degreeaward)
    {
        return response()->json(['data'=>$degreeaward]);
    }

    public function getDegreeAwardForType(DegreeType $degreetype)
    {
        return response()->json(['data'=>$degreetype->awards]);
    }

    public function getFeeItem(FeeItem $feeitem){
        
        return response()->json(['data'=>$feeitem]);
    }

    
}
