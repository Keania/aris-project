@extends('app')
@section('page.title')
Predata Set Up
@endsection

@section('page.content')
<div class="section__content">

    <div id="predata">
        <h3><i>Set up</i>Predefined Data</h3>

        <div id="add__predata" class="setup__divs">
            <h5>Pre data definition</h5>
            <form method="POST" action="{{route('save.predata')}}">
                @csrf

                <div class="form-group col-md-6 row">

                    <label class="form-label col-md-4">Select Category</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('cat_id') ? 'invalid' : ''}}" name="cat_id">
                            @if(count($categories) > 0)
                            <option value="null">--Select Category--</option>
                            @foreach($categories as $category)
                            <option value="{{$category->CategoryID}}">{{$category->DataCategory}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                        @if($errors->has('cat_id'))
                        <span class="errors">{{$errors->first('cat_id')}}</span>
                        @endif
                        </select>
                        @if($errors->has('cat_id'))
                        <span class="errors">{{$errors->first('cat_id')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Predata</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{$errors->has('predata') ? 'invalid' : ''}}" name="predata">
                        @if($errors->has('predata'))
                        <span class="errors">{{$errors->first('predata')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Add" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#edit__predata" value="Edit" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#delete__predata" value="Delete" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="edit__predata" class="collapse setup__divs">

            <h5>Pre data Modification</h5>
            <form method="POST" action="{{route('update.predata')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select Category</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('cat_id') ? 'invalid' : ''}} " name="cat_id" onchange="getPredatas(event)" id="categories">

                            @if(count($categories) > 0)
                            <option value="null">--Select Category--</option>
                            @foreach($categories as $category)
                            <option value="{{$category->CategoryID}}">{{$category->DataCategory}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                        @if($errors->has('cat_id'))
                        <span class="errors">{{$errors->first('cat_id')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select Predata</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('cat_id') ? 'invalid' : ''}} " name="predata_id" onchange="selectPredata(event)" id="predatas">
                            <option>None</option>
                            @if($errors->has('predata_id'))
                            <span class="errors">{{$errors->first('predata_id')}}</span>
                            @endif

                        </select>
                    </div>

                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Predata</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control  {{$errors->has('predata') ? 'invalid' : ''}} " name="data" id="editpredataInput">
                        @if($errors->has('predata'))
                        <span class="errors">{{$errors->first('predata')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Save" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="delete__predata" class="collapse setup__divs">
            <h5>Pre data deletion</h5>
            <form method="POST" action="{{route('delete.predata')}}">
                @csrf
                <div class="form-group col-md-6">
                    <label class="form-label">Select Predata</label>
                    <select class="form-control" name="predata_id">
                        @if(count($predatas) > 0)
                        @foreach($predatas as $predata)
                        <option value="{{$predata->DataID}}">{{$predata->Data}}</option>
                        @endforeach
                        @else
                        <option>No predata available</option>
                        @endif
                    </select>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Delete" class="btn btn-primary">

                </div>
            </form>
        </div>
    </div>

    <div id="faculty__colour">
        <h3><i>Set up</i>Faculty Colours</h3>

        <div id="add__predata" class="setup__divs">
            <h5>Pre data definition</h5>
            <form method="POST" action="{{route('save.faculty.colour')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Colour</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{$errors->has('colour_name') ? 'invalid' : ''}}" name="colour_name">
                        @if($errors->has('colour_name'))
                        <span class="errors">{{$errors->first('colour_name')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Add" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#edit__faculty_colour" value="Edit" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#delete__faculty_colour" value="Delete" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="edit__faculty_colour" class="collapse setup__divs">

            <h5>Pre data Modification</h5>
            <form method="POST" action="{{route('update.faculty.colour')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select Colour</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('colour_id') ? 'invalid' : ''}} " name="colour_id" onchange="getColour(event)" id="colourList">

                            @if(count($faculty_colours) > 0)
                            <option value="null">--Select Colour--</option>
                            @foreach($faculty_colours as $colour)
                            <option value="{{$colour->ColourID}}">{{$colour->ColourName}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                        @if($errors->has('colour_id'))
                        <span class="errors">{{$errors->first('colour_id')}}</span>
                        @endif
                    </div>

                </div>


                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Colour</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control  {{$errors->has('colour_name') ? 'invalid' : ''}} " name="colour_name" id="editColourNameInput">
                        @if($errors->has('colour_name'))
                        <span class="errors">{{$errors->first('colour_name')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Save" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="delete__faculty_colour" class="collapse setup__divs">
            <h5>Pre data deletion</h5>
            <form method="POST" action="{{route('delete.faculty.colour')}}">
                @csrf
                <div class="form-group col-md-6">
                    <label class="form-label">Select Colour</label>
                    <select class="form-control" name="colour_id">
                        @if(count($faculty_colours) > 0)
                        @foreach($faculty_colours as $colour)
                        <option value="{{$colour->ColourID}}">{{$colour->ColourName}}</option>
                        @endforeach
                        @else
                        <option>No colour available</option>
                        @endif
                    </select>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Delete" class="btn btn-primary">

                </div>
            </form>
        </div>
    </div>


</div>
@endsection
@section('page.script')
<script>
    var getPredatas = function(evt) {

        let select = evt.target.value;
        if (isNaN(select)) {

            return false;
        }
        $.getJSON('api/predata/category/' + select, function(response) {


        }).done(function(response) {
            var selecter = document.getElementById('predatas');
            selecter.innerHTML = "";
            response.data.forEach(function(predata) {
                console.log(predata)
                option = document.createElement('option')
                option.setAttribute('value', predata.DataID);
                option.innerHTML = predata.Data;
                selecter.appendChild(option);
            })

        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        });
    }


    var selectPredata = function(evt) {

        let select = evt.target.value;
        if (isNaN(select)) {
            return false;
        }
        $.getJSON('api/predata/' + select, function(response) {}).done(function(response) {
            document.getElementById('editpredataInput').value = response.data.Data
            console.log(response.data.Data);
        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        })

    }

    var getColour = function(evt) {

        let select = evt.target.value;
        if (isNaN(select)) {
            return false;
        }
        $.getJSON('api/facultycolour/' + select, function(response) {}).done(function(response) {
            document.getElementById('editColourNameInput').value = response.data.ColourName
            console.log(response.data.Data);
        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        })

    }
</script>
@endsection