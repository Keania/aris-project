<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OlevelSubject extends Model
{
    //
    protected $guarded = [];
    protected $table = 'tblsubjects';
    public $timestamps = false;


    public function getRouteKeyName()
    {
        return 'SubjectID';
    }

    public function getKeyName()
    {
        return 'SubjectID';
    }
}
