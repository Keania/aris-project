<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProgramType;
use App\DegreeType;
use Illuminate\Database\QueryException;
use Exception;

class ProgramTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $program_types = ProgramType::all();
        $degree_types = DegreeType::all();
        return view('setup.program_type',['program_types'=>$program_types,'degree_types'=>$degree_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'programtype' => 'required',
            'programtypecode'=>'required'
        ]);

        $programtype = $request->programtype;
        $programtypecode = $request->programtypecode;
        try {

            if (ProgramType::where('ProgramType', $programtype)->where('ProgramTypeCode',$programtypecode)->exists()) {

                throw new Exception('Program already exists in database');
            }

            ProgramType::create([
                'ProgramType'=>$programtype,
                'ProgramTypeCode'=>$programtypecode
            ]);
           
        } catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Program successfully added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $request->validate([
            'programtype'=>'required',
            'programtype_id'=>'required',
            'programtypecode'=>'required'
        ]);
    
        
        try {

            $programtype = ProgramType::where('ProgramTypeID',$request->programtype_id)->first();
    
            if(ProgramType::where('ProgramType',$request->programtype)->where('ProgramTypeCode',$request->programtypecode)->exists()){
                throw new Exception('The new Program Type already exists in the database');
            }
    
    
            $programtype->update(['ProgramType'=>$request->programtype,'ProgramTypeCode'=>$request->programtypecode]);
    
        }catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Program successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $request->validate([
            'programtype_id'=>'required',
        ]);

        try {

            $programtype = ProgramType::where('ProgramTypeID',$request->programtype_id)->first();
            $programtype->delete();
    
        }catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Program Type successfully deleted']);
    }
}
