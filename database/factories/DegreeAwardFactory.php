<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\DegreeAward;
use Faker\Generator as Faker;
use App\DegreeType;

$factory->define(DegreeAward::class, function (Faker $faker) {
    $degree_type  = DegreeType::all()->pluck('DegreeTypeID');
    return [
        //
        'DegreeAward'=>$faker->sentence(2),
        'DegreeAwardCode'=>$faker->sentence(2),
        'DegreeTypeID'=>$faker->randomElement($degree_type)
    ];
});
