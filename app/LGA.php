<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LGA extends Model
{
    //
    protected $guarded = [];
    protected $table = 'tbllga';
    public $timestamps = false;

    public function students()
    {
        return $this->hasMany('App\Student','LGAID','LGAID');
    }

    public function user_school()
    {
        return $this->hasMany('App\UserSchool','LGAID','LGAID');
    }

    public function getRouteKeyName()
    {
        return 'LGAID';
    }

    public function getKeyName()
    {
        return 'LGAID';
    }
}
