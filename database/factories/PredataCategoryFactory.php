<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\PredataCategory;
use Faker\Generator as Faker;

$factory->define(PredataCategory::class, function (Faker $faker) {
    return [
        //
        'DataCategory'=>$faker->sentence(2)
    ];
});
