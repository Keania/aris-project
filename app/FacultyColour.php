<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacultyColour extends Model
{
    //

    protected $guarded = [];
    protected $table = 'tblcolour';
    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'ColourID';
    }


    public function appliedFaculty()
    {
        return $this->hasMany('App\Faculty','FacultyColourID');
    }

    public function getKeyName()
    {
        return 'ColourID';
    }
}

