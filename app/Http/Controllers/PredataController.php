<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PreData;
use App\PredataCategory;
use Illuminate\Database\QueryException;
use Exception;
use Throwable;
use App\FacultyColour;
use Illuminate\Support\Facades\DB;

class PredataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = PredataCategory::all();
        $predatas = Predata::all();
        $faculty_colours = FacultyColour::all();
        return view('setup.predata',['categories'=>$categories,'predatas'=>$predatas,'faculty_colours'=>$faculty_colours]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'cat_id'=>'required',
            'predata'=>'required',
        ]);

        $cat_id = $request->cat_id;
        $predata = $request->predata;

       

        try {

            if(PreData::where('CategoryID',$cat_id)->where('Data',$predata)->exists()){

                throw new Exception('Predata already exists in database');
            }
          
           $this->storeNewPredata($cat_id,$predata);

        }catch(QueryException | Exception $e){
            return redirect()->back()->with(['error'=>isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success'=>'Predata successfully added']);
    }


    public function storeNewPredata($cat_id,$predata_arg)
    {
      
        $predata = new Predata();
        $predata->CategoryID = $cat_id;
        $predata->Data = $predata_arg;
        $predata->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'cat_id'=>'required',
            'data'=>'required',
            'predata_id'=>'required'
        ]);
        //

       
        
        try {

            $preData = PreData::where('DataID',$request->predata_id)->first();
           
           
            if(PreData::where('CategoryID',$request->cat_id)->where('Data',$request->data)->exists()){

                throw new Exception('Predata with name already exists in database');
            }

            !isset($request->data) ?: $preData->Data = $request->data ;
            !isset($request->cat_id) ?: $preData->CategoryID = $request->cat_id ;
   
            $preData->save();

        }catch(QueryException | Exception $e){
    
            return redirect()->back()->with(['error'=>isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
      
        return redirect()->back()->with(['success'=>'Predata changes successfully added']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $request->validate([
            'predata_id'=>'required'
        ]);
      
        try {

           $predata = Predata::where('DataID',$request->predata_id)->first();
           $predata->delete();
          
        }catch(QueryException | Exception $e){
           
            return redirect()->back()->with(['error'=>isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success'=>'Predata was deleted']);
    }
}
