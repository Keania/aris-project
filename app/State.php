<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    protected $guarded = [];
    protected $table = 'tblstate';
    public $timestamps = false;

    public function lgas()
    {
        return $this->hasMany('App\LGA','StateID','StateID');
    }

    public function schools()
    {
        return $this->hasMany('App\School','StateID','StateID');
    }
    public function getRouteKeyName()
    {
        return 'StateID';
    }

    public function getKeyName()
    {
        return 'StateID';
    }
}
