<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/predata','PredataController@index')->name('predata.setup');
Route::get('/state/lga','StateController@index')->name('state_lga.setup');
Route::get('/olevel','OlevelController@index')->name('olevelsubject.setup');
Route::get('fee/item','FeeItemController@index')->name('fee.item.setup');
Route::get('program/degree/type','ProgramTypeController@index')->name('program.type.setup');
Route::get('degreeaward','DegreeAwardController@index')->name('degreeaward.setup');


Route::post('/save/predata','PredataController@store')->name('save.predata');
Route::post('/edit/predata','PredataController@update')->name('update.predata');
Route::post('/delete/predata','PredataController@destroy')->name('delete.predata');

Route::post('/save/facultycolour','FacultyColourController@store')->name('save.faculty.colour');
Route::post('/update/facultycolour','FacultyColourController@update')->name('update.faculty.colour');
Route::post('/delete/facultycolour','FacultyColourController@destroy')->name('delete.faculty.colour');

Route::post('/save/lga','LGAController@store')->name('save.lga');
Route::post('/update/lga','LGAController@update')->name('update.lga');
Route::post('/delete/lga','LGAController@destroy')->name('delete.lga');

Route::post('/save/state','StateController@store')->name('save.state');
Route::post('/update/state','StateController@update')->name('update.state');
Route::post('/delete/state','StateController@destroy')->name('delete.state');

Route::post('/save/olevelsubject','OlevelController@store')->name('save.olevelsubject');
Route::post('/update/olevelsubject','OlevelController@update')->name('update.olevelsubject');
Route::post('/delete/olevelsubject','OlevelController@destroy')->name('delete.olevelsubject');

Route::post('/save/feeItem','FeeItemController@store')->name('save.feeItem');
Route::post('/update/feeItem','FeeItemController@update')->name('update.feeItem');
Route::post('/delete/feeItem','FeeItemController@destroy')->name('delete.feeItem');

Route::post('/save/programType','ProgramTypeController@store')->name('save.programType');
Route::post('/update/programType','ProgramTypeController@update')->name('update.programType');
Route::post('/delete/programType','ProgramTypeController@destroy')->name('delete.programType');

Route::post('/save/degreeType','DegreeTypeController@store')->name('save.degreeType');
Route::post('/update/degreeType','DegreeTypeController@update')->name('update.degreeType');
Route::post('/delete/degreeType','DegreeTypeController@destroy')->name('delete.degreeType');

Route::post('/save/degreeAward','DegreeAwardController@store')->name('save.degreeAward');
Route::post('/update/degreeAward','DegreeAwardController@update')->name('update.degreeAward');
Route::post('/delete/degreeAward','DegreeAwardController@destroy')->name('delete.degreeAward');

