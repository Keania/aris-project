<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\State;

class StateTest extends TestCase
{
    /**
     * Creating state requires state value
     *
     * @return void
     */
    public function testCreateStateRequiresState()
    {
        $response = $this->post('/save/state');
        $response->assertStatus(302)->assertSessionHasErrors(['state']);
    }

    /**
     * Creating state success
     *
     * @return void
     */
    public function testCreateStateSuccess()
    {
        $state = factory(State::class)->make();
        $response = $this->post('/save/state',['state'=>$state->State]);
        $response->assertStatus(302)->assertSessionHas(['success']);
    }

    /**
     * Creating state that exists
     *
     * @return void
     */
    public function testCreateAlreadyExists()
    {
        $state = State::all()->random(1)->first();
        $response = $this->post('/save/state',['state'=>$state->State]);
        $response->assertStatus(302)->assertSessionHas(['error'=>'State already exists in database']);
    }

     /**
     * Edit to  state that exists
     *
     * @return void
     */
    public function testEditToStateAlreadyExists()
    {
        $state1 = State::all()->random(1)->first();
        $state2 = State::all()->random(1)->first();
        $response = $this->post('/update/state',['state'=>$state1->State,'state_id'=>$state2->StateID]);
        $response->assertStatus(302)->assertSessionHas(['error'=>'The new state name already exists in the database']);
    }

     /**
     * Delete requires state ID
     *
     * @return void
     */
    public function testDeleteRequiresStateID()
    {
        $response = $this->post('/delete/state',[]);
        $response->assertStatus(302)->assertSessionHasErrors(['state_id']);
    }

    /**
     * Success Delete
     *
     * @return void
     */
    // public function testDeleteSuccess()
    // {
    //     $state = State::all()->random(1)->first();
    //     $response = $this->post('/delete/state',['state_id'=>$state->StateID]);
    //     $response->assertStatus(302)->assertSessionHas(['success']);
    // }


     /**
     * Get JSON State LGA
     *
     * @return void
     */
    public function testGetJSONStateLGA()
    {
        $state = State::all()->random(1)->first();
        $response = $this->json('GET',"/api/lga/state/{$state->StateID}");
        $response->assertStatus(200)->assertJsonFragment(['data'=>$state->lgas->toArray()]);
    }

     /**
     * Get JSON State 
     *
     * @return void
     */
    public function testGetJSONState()
    {
        $state = State::all()->random(1)->first();
        $response = $this->json('GET',"/api/state/{$state->StateID}");
        $response->assertStatus(200)->assertJsonFragment(['data'=>$state->toArray()]);
    }


    

    
  



    

    
}
