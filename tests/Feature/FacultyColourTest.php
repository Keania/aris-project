<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\FacultyColour;

class FacultyColourTest extends TestCase
{
    /**
     * Creating requires colour name.
     *
     * @return void
     */
    public function testCreateColourRequiresColourName()
    {
        $response = $this->post('/save/facultycolour');
        $response->assertStatus(302)->assertSessionHasErrors(['colour_name']);
    }

    /**
     * Successful creation
     *
     * @return void
     */
    public function testCreateColourSuccess()
    {
        $colour = factory(FacultyColour::class)->make();
        $response = $this->post('/save/facultycolour',['colour_name'=>$colour->ColourName]);
        $response->assertStatus(302)->assertSessionHasNoErrors();
    }

    /**
     * Adding a colour name that already exists
     *
     * @return void
     */
    public function testCreateAlreadyExistsColourName()
    {
        $colour = FacultyColour::all()->random(1)->first();
        $response = $this->post('/save/facultycolour',['colour_name'=>$colour->ColourName]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }

    /**
     * Editing to a name that already exists
     *
     * @return void
     */
    public function testEditToAlreadyExistsColourName()
    {
        $colour1 = FacultyColour::all()->random(1)->first();
        $colour2 = FacultyColour::all()->random(1)->first();
        $response = $this->post('/update/facultycolour',['colour_name'=>$colour1->ColourName,'colour_id'=>$colour2->ColourID]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }

     /**
     * Deleting a colour requires color id.
     *
     * @return void
     */
    public function testDeleteRequiresColourID()
    {
        $response = $this->post('/delete/facultycolour',[]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }

     /**
     * Successful delete
     *
     * @return void
     */
    public function testDeleteSuccess()
    {
        $colour2 = FacultyColour::all()->random(1)->first();
        $response = $this->post('/delete/facultycolour',['colour_id'=>$colour2->ColourID]);
        $response->assertStatus(302)->assertSessionHas('success');
    }

     /**
     * Get JSON Faculty Colour
     *
     * @return void
     */
    public function testGetJSONFacultyColour()
    {
        $facultycolour = FacultyColour::all()->random(1)->first();
        $response = $this->json('GET',"/api/facultycolour/{$facultycolour->ColourID}");
        $response->assertStatus(200)->assertJsonFragment(['data'=>$facultycolour->toArray()]);
    }

    
}
