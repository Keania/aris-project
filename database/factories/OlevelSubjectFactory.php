<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OlevelSubject;
use Faker\Generator as Faker;

$factory->define(OlevelSubject::class, function (Faker $faker) {
    return [
        //
        'Subject'=>$faker->sentence(2)
    ];
});
