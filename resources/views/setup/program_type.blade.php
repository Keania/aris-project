@extends('app')
@section('page.title')
Program & Degree Type Set Up
@endsection

@section('page.content')
<div class="section__content">

    <div id="degreetype">
        <h3><i>Set up</i> Degree / Progrma Type</h3>

        <div id="add__degreeType" class="setup__divs">
            <h5>New Degree definition</h5>
            <form method="POST" action="{{route('save.degreeType')}}">
                @csrf

               
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Degree type</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{$errors->has('degreetype') ? 'invalid' : ''}}" name="degreetype">
                        @if($errors->has('degreetype'))
                        <span class="errors">{{$errors->first('degreetype')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Add" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#edit__degreetype" value="Edit" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#delete__degreetype" value="Delete" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="edit__degreetype" class="collapse setup__divs">

            <h5>Degree Type Modification</h5>
            <form method="POST" action="{{route('update.degreeType')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select Degree Type</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('degreetype_id') ? 'invalid' : ''}} " name="degreetype_id" onchange="getDegreeType(event)">

                            @if(count($degree_types) > 0)
                            <option value="null">--Select Degree Type--</option>
                            @foreach($degree_types as $degreetype)
                            <option value="{{$degreetype->DegreeTypeID}}">{{$degreetype->DegreeType}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                        @if($errors->has('degreetype_id'))
                        <span class="errors">{{$errors->first('degreetype_id')}}</span>
                        @endif
                    </div>

                </div>

              

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Degree Type</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control  {{$errors->has('degreetype') ? 'invalid' : ''}} " name="degreetype" id="editDegreeTypeInput">
                        @if($errors->has('degreetype'))
                        <span class="errors">{{$errors->first('degreetype')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Save" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="delete__degreetype" class="collapse setup__divs">
            <h5>Degree Type deletion</h5>
            <form method="POST" action="{{route('delete.degreeType')}}">
                @csrf
                <div class="form-group col-md-6">
                    <label class="form-label">Select Degree Type</label>
                    <select class="form-control" name="degreetype_id">
                        @if(count($degree_types) > 0)
                        @foreach($degree_types as $degree_type)
                        <option value="{{$degree_type->DegreeTypeID}}">{{$degree_type->DegreeType}}</option>
                        @endforeach
                        @else
                        <option>No data available</option>
                        @endif
                    </select>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Delete" class="btn btn-primary">

                </div>
            </form>
        </div>
    </div>

    <div id="programtype">
        <h3><i>Set up</i>Program Type</h3>

        <div id="add__programtype" class="setup__divs">
            <h5>Program Type definition</h5>
            <form method="POST" action="{{route('save.programType')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Program Type </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{$errors->has('programtype') ? 'invalid' : ''}}" name="programtype">
                        @if($errors->has('programtype'))
                        <span class="errors">{{$errors->first('programtype')}}</span>
                        @endif
                    </div>

                </div>
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Program Type Code </label>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{$errors->has('programtypecode') ? 'invalid' : ''}}" name="programtypecode">
                        @if($errors->has('programtypecode'))
                        <span class="errors">{{$errors->first('programtypecode')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Add" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#edit__programtype" value="Edit" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#delete__programtype" value="Delete" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="edit__programtype" class="collapse setup__divs">

            <h5>Program Type Modification</h5>
            <form method="POST" action="{{route('update.programType')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select Program Type</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('programtype_id') ? 'invalid' : ''}} " name="programtype_id" onchange="getProgramType(event)" id="stateList">

                            @if(count($program_types) > 0)
                            <option value="null">--Select Program Type--</option>
                            @foreach($program_types as $program_type)
                            <option value="{{$program_type->ProgramTypeID}}">{{$program_type->ProgramType}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                        @if($errors->has('programtype_id'))
                        <span class="errors">{{$errors->first('programtype_id')}}</span>
                        @endif
                    </div>

                </div>


                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Program Type</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control  {{$errors->has('programtype') ? 'invalid' : ''}} " name="programtype" id="editProgramTypeInput">
                        @if($errors->has('programtype'))
                        <span class="errors">{{$errors->first('programtype')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Program Type Code</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control  {{$errors->has('programtypecode') ? 'invalid' : ''}} " name="programtypecode" id="editProgramTypeCodeInput">
                        @if($errors->has('programtypecode'))
                        <span class="errors">{{$errors->first('programtypecode')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Save" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="delete__programtype" class="collapse setup__divs">
            <h5>Program deletion</h5>
            <form method="POST" action="{{route('delete.programType')}}">
                @csrf
                <div class="form-group col-md-6">
                    <label class="form-label">Select Program Type</label>
                    <select class="form-control" name="programtype_id">
                        @if(count($program_types) > 0)
                        @foreach($program_types as $program_type)
                        <option value="{{$program_type->ProgramTypeID}}">{{$program_type->ProgramType}}</option>
                        @endforeach
                        @else
                        <option>No colour available</option>
                        @endif
                    </select>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Delete" class="btn btn-primary">

                </div>
            </form>
        </div>
    </div>


</div>
@endsection
@section('page.script')
<script>
    var getDegreeType = function(evt) {

        let select = evt.target.value;
        if (isNaN(select)) {
            return false;
        }
        $.getJSON('/api/degreetype/' + select, function(response) {}).done(function(response) {
            document.getElementById('editDegreeTypeInput').value = response.data.DegreeType
            
        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        })

    }

    var getProgramType = function(evt) {

        let select = evt.target.value;
        console.log(select)
        if (isNaN(select)) {
            return false;
        }
        $.getJSON('/api/programtype/' + select, function(response) {}).done(function(response) {
            document.getElementById('editProgramTypeInput').value = response.data.ProgramType;
            document.getElementById('editProgramTypeCodeInput').value = response.data.ProgramTypeCode;
        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        })

    }
</script>
@endsection