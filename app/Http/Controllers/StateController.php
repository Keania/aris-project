<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
use Illuminate\Database\QueryException;
use Exception;
use App\LGA;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $states = State::all();
        $lgas = LGA::all();
        return view('setup.states',['states'=>$states,'lgas'=>$lgas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'state' => 'required',
        ]);

        $state = $request->state;
        try {

            if (State::where('State', $state)->exists()) {

                throw new Exception('State already exists in database');
            }

            State::create([
                'State'=>$state,
            ]);
           
        } catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'State successfully added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'state'=>'required',
            'state_id'=>'required'
        ]);
    
        
        try {

            $state = State::where('StateID',$request->state_id)->first();
    
            if(State::where('State',$request->state)->exists()){
                throw new Exception('The new state name already exists in the database');
            }
    
            if(optional($state->lgas)->isNotEmpty() || optional($state->schools)->isNotEmpty()){
    
                throw new Exception('This state is attached to sensitive data and cannot be altered');
            }
    
            $state->update(['State'=>$request->state]);
    
        }catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'State successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $request->validate([
            'state_id'=>'required',
        ]);

        try {

            $state = State::where('StateID',$request->state_id)->first();
          

            if(optional($state->lgas)->isNotEmpty() || optional($state->schools)->isNotEmpty()){
                
            
                throw new Exception('This state is attached to other records and cannot be deleted');
            }
    
            $state->delete();
          
    
        }catch (QueryException | Exception $e) {
          
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'State successfully deleted']);

    }
}
