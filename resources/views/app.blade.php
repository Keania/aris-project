<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('page.title')</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>

<body>
    <div id="app">
        <div class="container">
            <header>
                <div id="top__header" style="margin-bottom:20px;">
                    <img src="{{asset('uniport.jpg')}}" style="width:30%">
                    <br>
                </div>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                   
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">

                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="{{url('/')}}" class="nav-link">Home</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Dashboard</a>
                            </li>
                            <li class="nav-item active">
                                <a href="#" class="nav-link">Set Up</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Faculty</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Department</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Admission</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Result</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Fee</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Info</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Document</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Admin</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Logout</a>
                            </li>


                        </ul>

                    </div>
                </nav>

            </header>
            @if(session()->has('error'))
            <div class="alert alert-danger">{{session()->get('error')}}</div>
            @endif
            @if(session()->has('success'))
            <div class="alert alert-success">{{session()->get('success')}}</div>
            @endif
            <div id="errorBag">
                
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $err)
                                <li>{{$err}}</li>
                            @endforeach
                        </ul>
                    </div>
                   
                @endif
            </div>
           
            <section id="main">
                <div class="alert alert-danger display__none" id="ajax-error"></div>
                @yield('page.content')
            </section>
        </div>
    </div>

    <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script>
    <script>
        $(document).ajaxError(function(event,request,settings){
            $('#ajax-error').removeClass('display__none');
           
        })
    </script>
    @yield('page.script')
</body>

</html>