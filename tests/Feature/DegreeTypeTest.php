<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\DegreeType;

class DegreeTypeTest extends TestCase
{
    /**
     * Create Requires Degree Type
     *
     * @return void
     */
    public function testCreateRequiresDegreeType()
    {
        $response = $this->post('/save/degreeType',[]);
        $response->assertStatus(302)->assertSessionHasErrors(['degreetype']);
    }


     /**
     * Create Success Degree Type
     *
     * @return void
     */
    public function testCreateSuccessDegreeType()
    {
        $degreetype = factory(DegreeType::class)->make();
        $response = $this->post('/save/degreeType',['degreetype'=>$degreetype->DegreeType]);
        $response->assertStatus(302)->assertSessionHas(['success']);
    }


     /**
     * Create Already Exists Degree Type
     *
     * @return void
     */
    public function testCreateAlreadyExistsDegreeType()
    {
        $degreetype = factory(DegreeType::class)->create();
        $response = $this->post('/save/degreeType',['degreetype'=>$degreetype->DegreeType]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }


     /**
     * Update Already Exists Degree Type
     *
     * @return void
     */
    public function testUpdateAlreadyExistsDegreeType()
    {
        $degreetype_db = DegreeType::all()->random(1)->first();
        $degreetype = factory(DegreeType::class)->create();
        $response = $this->post('/update/degreeType',['degreetype'=>$degreetype->DegreeType,'degreetype_id'=>$degreetype_db->DegreeTypeID]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }


     /**
     * Delete Requires DegreeTypeID
     *
     * @return void
     */
    public function testDeleteRequiresDegreeTypeID()
    {
        $response = $this->post('/delete/degreeType',[]);
        $response->assertStatus(302)->assertSessionHasErrors('degreetype_id');
    }

     /**
     * Delete Success
     *
     * @return void
     */
    public function testDeleteSuccessDegreeType()
    {
        $degreetype = factory(DegreeType::class)->create();
        $response = $this->post('/delete/degreeType',['degreetype_id'=>$degreetype->DegreeTypeID]);
        $response->assertStatus(302)->assertSessionHas(['success']);
    }

     /**
     * Get JSON Degree Award
     *
     * @return void
     */
    public function testGetJSONDegreeTypeDegreeAward()
    {
        $degreetype = DegreeType::all()->random(1)->first();
        $response = $this->json('GET',"/api/degreeaward/degreetype/{$degreetype->DegreeTypeID}");
        $response->assertStatus(200)->assertJsonFragment(['data'=>$degreetype->awards->toArray()]);
    }


     /**
     * Get JSON Degree Type
     *
     * @return void
     */
    public function testGetJSONDegreeType()
    {
        $degreetype = DegreeType::all()->random(1)->first();
        $response = $this->json('GET',"/api/degreetype/{$degreetype->DegreeTypeID}");
        $response->assertStatus(200)->assertJsonFragment(['data'=>$degreetype->toArray()]);
    }
}
