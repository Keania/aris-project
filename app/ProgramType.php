<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramType extends Model
{
    //
    protected $guarded = [];
    protected $table = 'tblprogramtype';
    public $timestamps = false;


    public function getRouteKeyName()
    {
        return 'ProgramTypeID';
    }

    public function getKeyName()
    {
        return 'ProgramTypeID';
    }
}
