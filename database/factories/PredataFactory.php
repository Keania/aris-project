<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Predata;
use Faker\Generator as Faker;
use App\PredataCategory;

$factory->define(Predata::class, function (Faker $faker) {

    $categories = PredataCategory::all()->pluck('CategoryID');
    return [
        'CategoryID'=>$faker->randomElement($categories),
        'Data'=>$faker->sentence(2)
    ];
});
