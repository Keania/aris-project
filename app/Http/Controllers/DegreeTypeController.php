<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DegreeType;
use Illuminate\Database\QueryException;
use Exception;
class DegreeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'degreetype' => 'required',
        ]);

        $degreetype = $request->degreetype;
      
        try {

            if (DegreeType::where('DegreeType', $degreetype)->exists()) {

                throw new Exception('Degree already exists in database');
            }

            DegreeType::create([
                'DegreeType' => $degreetype,
            ]);
        } catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Degree Type successfully added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $request->validate([
            'degreetype' => 'required',
            'degreetype_id' => 'required',
        ]);


        try {

            $degreetype = DegreeType::where('DegreeTypeID', $request->degreetype_id)->first();
            if($degreetype->awards->isNotEmpty()){
                throw new Exception('This degree type is attached to sensitive data and cannot be edited');
            }
            if (DegreeType::where('DegreeType', $request->degreetype)->exists()) {
                throw new Exception('The new Degree  Type already exists in the database');
            }


            $degreetype->update(['DegreeType' => $request->degreetype]);
        } catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Degree successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $request->validate([
            'degreetype_id' => 'required',
        ]);

        try {
            $degreetype = DegreeType::where('DegreeTypeID', $request->degreetype_id)->first();
            if($degreetype->awards->isNotEmpty()){
                throw new Exception('This degree type is attached to sensitive data and cannot be deleted');
            }

           
            $degreetype->delete();
        } catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Degree Type successfully deleted']);
    }
}
