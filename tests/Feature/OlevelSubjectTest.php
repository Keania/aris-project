<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\OlevelSubject;

class OlevelSubjectTest extends TestCase
{
    /**
     * Create Requires olevelsubject
     *
     * @return void
     */
    public function testCreateRequiresOlevelSubject()
    {
        $response = $this->post('/save/olevelsubject');
        $response->assertStatus(302)->assertSessionHasErrors(['subject']);
    }

     /**
     * Create Success olevelsubject
     *
     * @return void
     */
    public function testCreateSuccessOlevelSubject()
    {
        $olevelsubject = factory(OlevelSubject::class)->make();
        $response = $this->post('/save/olevelsubject',['subject'=>$olevelsubject->Subject]);
        $response->assertStatus(302)->assertSessionHas(['success']);
    }

     /**
     * Create Already Exists olevelsubject
     *
     * @return void
     */
    public function testCreateAlreadyExistsOlevelSubject()
    {
        $olevelsubject = factory(OlevelSubject::class)->create();
        $response = $this->post('/save/olevelsubject',['subject'=>$olevelsubject->Subject]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }

     /**
     * Update Already Exists olevelsubject
     *
     * @return void
     */
    public function testUpdateToAlreadyExistsOlevelSubject()
    {
        $olevelsubject = factory(OlevelSubject::class)->create();
        $olevelsubject_db = OlevelSubject::all()->random(1)->first();
        $response = $this->post('/update/olevelsubject',['subject'=>$olevelsubject->Subject,'subject_id'=>$olevelsubject_db->SubjectID]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }

     /**
     * Delete Requires olevelsubject
     *
     * @return void
     */
    public function testDeleteRequiresOlevelSubjectID()
    {
        $response = $this->post('/delete/olevelsubject');
        $response->assertStatus(302)->assertSessionHasErrors('subject_id');
    }

     /**
     * Delete Success olevelsubject
     *
     * @return void
     */
    public function testDeleteSuccess()
    {
        $olevelsubject = factory(OlevelSubject::class)->create();
        $response = $this->post('/delete/olevelsubject',['subject_id'=>$olevelsubject->SubjectID]);
        $response->assertStatus(302)->assertSessionHas('success');
    }


     /**
     * Get JSON OlevelSubject
     *
     * @return void
     */
    public function testGetJSONOlevelSubject()
    {
        $olevelsubject = OlevelSubject::all()->random(1)->first();
        $response = $this->json('GET',"/api/olevelsubject/{$olevelsubject->SubjectID}");
        $response->assertStatus(200)->assertJsonFragment(['data'=>$olevelsubject->toArray()]);
    }
}
