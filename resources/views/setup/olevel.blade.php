@extends('app')
@section('page.title')
Olevel Subject Set Up
@endsection

@section('page.content')
<div class="section__content">

    <div id="olevelsubjects">
        <h3><i>Set up</i>Olevel Result</h3>

        <div id="add__olevelsubject" class="setup__divs">
            <h5>Olevel Subject definition</h5>
            <form method="POST" action="{{route('save.olevelsubject')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Subject</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{$errors->has('subject') ? 'invalid' : ''}}" name="subject">
                        @if($errors->has('subject'))
                        <span class="errors">{{$errors->first('subject')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Add" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#edit__olevelsubject" value="Edit" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#delete__olevelsubject" value="Delete" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="edit__olevelsubject" class="collapse setup__divs">

            <h5>Olevel Subject Modification</h5>
            <form method="POST" action="{{route('update.olevelsubject')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select Subject</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('subject_id') ? 'invalid' : ''}} " name="subject_id" onchange="getSubject(event)" id="subjectList">

                            @if(count($subjects) > 0)
                            <option value="null">--Select Subject--</option>
                            @foreach($subjects as $subject)
                            <option value="{{$subject->SubjectID}}">{{$subject->Subject}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                        @if($errors->has('subject_id'))
                        <span class="errors">{{$errors->first('subject_id')}}</span>
                        @endif
                    </div>

                </div>


                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Subject</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control  {{$errors->has('subject') ? 'invalid' : ''}} " name="subject" id="editOlevelSubjectInput">
                        @if($errors->has('subject'))
                        <span class="errors">{{$errors->first('subject')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Save" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="delete__olevelsubject" class="collapse setup__divs">
            <h5>Olevel Subject deletion</h5>
            <form method="POST" action="{{route('delete.olevelsubject')}}">
                @csrf
                <div class="form-group col-md-6">
                    <label class="form-label">Select Olevel Subject</label>
                    <select class="form-control" name="subject_id">
                        @if(count($subjects) > 0)
                        @foreach($subjects as $subject)
                        <option value="{{$subject->SubjectID}}">{{$subject->Subject}}</option>
                        @endforeach
                        @else
                        <option>No colour available</option>
                        @endif
                    </select>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Delete" class="btn btn-primary">

                </div>
            </form>
        </div>

        <div class="col-md-6">
            <table class="table">
                <thead>
                    <tr>
                        <th>Subject</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($subjects as $subject)
                        <tr>
                            <td>{{$subject->Subject}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


</div>
@endsection
@section('page.script')
<script>
  

    var getSubject = function(evt) {

        let select = evt.target.value;
        console.log(select)
        if (isNaN(select)) {
            return false;
        }
        $.getJSON('/api/olevelsubject/' + select, function(response) {}).done(function(response) {
            document.getElementById('editOlevelSubjectInput').value = response.data.Subject
           
        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        })

    }
</script>
@endsection