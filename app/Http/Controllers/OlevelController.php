<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OlevelSubject;
use Exception;
use Illuminate\Database\QueryException;

class OlevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $subjects = OlevelSubject::all();
        return view('setup.olevel',['subjects'=>$subjects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'subject' => 'required',
        ]);

        $subject = $request->subject;
        try {

            if (OlevelSubject::where('Subject', $subject)->exists()) {

                throw new Exception('Subject already exists in database');
            }

            OlevelSubject::create([
                'Subject'=>$subject,
            ]);
           
        } catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Subject successfully added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $request->validate([
            'subject'=>'required',
            'subject_id'=>'required'
        ]);
    
        
        try {

            $subject = OlevelSubject::where('SubjectID',$request->subject_id)->first();
    
            if(OlevelSubject::where('Subject',$request->subject)->exists()){
                throw new Exception('The new suject name already exists in the database');
            }
    
    
            $subject->update(['Subject'=>$request->subject]);
    
        }catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Subject successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $request->validate([
            'subject_id'=>'required',
        ]);

        try {

            $subject = OlevelSubject::where('SubjectID',$request->subject_id)->first();
            $subject->delete();
    
        }catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Subject successfully deleted']);
    }
}
