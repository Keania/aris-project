<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Exception;
use App\LGA;

class LGAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'lga' => 'required',
            'state_id' => 'required',
        ]);

        $lga = $request->lga;
        $state_id = $request->state_id;

        try {

            if (LGA::where('LGA', $lga)->where('StateID', $state_id)->exists()) {

                throw new Exception('Local Government Area already exists in database');
            }

            LGA::create([
                'LGA' => $lga,
                'StateID' => $state_id,
            ]);
        } catch (QueryException | Exception $e) {
           
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Local Government Area successfully added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'state_id' => 'required',
            'lga'=>'required',
            'lga_id'=>'required'
        ]);

        try {

            $lga = LGA::where('LGAID',$request->lga_id)->first();

            if (LGA::where('LGA', $request->lga)->where('StateID',$request->state_id)->exists()) {
                throw new Exception('The new Local Government Area name already exists in the database');
            }
            $lga->update(['LGA' => $request->lga]);

        } catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Local Government Area successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
       
        $request->validate([
            'lga_id'=>'required'
        ]);

        try {

            $lga = LGA::where('LGAID',$request->lga_id)->first();
          
           
            if($lga->students->isNotEmpty() || $lga->user_school->isNotEmpty()){
               
                throw new Exception('This Local Government Area is attached to other records and cannot be deleted');
            }
           
           
            $lga->delete();
    
        }catch (QueryException | Exception $e) {
            return redirect()->back()->with(['error' => isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'Local Government successfully deleted']);
    }
}
