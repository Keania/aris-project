<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DegreeType extends Model
{
    //

    protected $guarded = [];
    protected $table = 'tbldegreetype';
    public $timestamps = false;


    public function getRouteKeyName()
    {
        return 'DegreeTypeID';
    }

    public function getKeyName()
    {
        return 'DegreeTypeID';
    }

    public function awards()
    {
        return $this->hasMany('App\DegreeAward','DegreeTypeID','DegreeTypeID');
    }
}
