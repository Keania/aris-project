<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PredataCategory extends Model
{
    //

    protected $guarded = [];
    protected $table = 'tblpredatacategory';
    public $timestamps = false;


    public function predatas()
    {
        return $this->hasMany('App\Predata','CategoryID','CategoryID');
    }

   


    public function getRouteKeyName()
    {
        return 'CategoryID';
    }
}
