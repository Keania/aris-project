<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\FacultyColour;
use Faker\Generator as Faker;

$factory->define(FacultyColour::class, function (Faker $faker) {
    return [
        //
        'ColourName'=>$faker->colorName
    ];
});
