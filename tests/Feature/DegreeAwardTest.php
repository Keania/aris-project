<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\DegreeAward;

class DegreeAwardTest extends TestCase
{
    /**
     * Create Requires Degree Type ID
     *
     * @return void
     */
    public function testCreateRequiresDegreeTypeID()
    {
        $degreeaward = factory(DegreeAward::class)->make();
        $response = $this->post('/save/degreeAward',['degreeward'=>$degreeaward->DegreeAward,'degreeawardcode'=>$degreeaward->DegreeAwardCode]);
        $response->assertStatus(302)->assertSessionHasErrors(['degreetype_id']);
    }


     /**
     * Create Success Degree Award
     *
     * @return void
     */
    public function testCreateSuccessDegreeAward()
    {
        $degreeaward = factory(DegreeAward::class)->make();
        $response = $this->post('/save/degreeAward',['degreeaward'=>$degreeaward->DegreeAward,'degreeawardcode'=>$degreeaward->DegreeAwardCode,'degreetype_id'=>$degreeaward->DegreeTypeID]);
        $response->assertStatus(302)->assertSessionHas(['success']);
    }


     /**
     * Create Already Exists Degree Award
     *
     * @return void
     */
    public function testCreateAlreadyExistsDegreeAward()
    {
       
        $degreeaward = factory(DegreeAward::class)->create();
        $response = $this->post('/save/degreeAward',['degreeaward'=>$degreeaward->DegreeAward,'degreeawardcode'=>$degreeaward->DegreeAwardCode,'degreetype_id'=>$degreeaward->DegreeTypeID]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }

     /**
     * Update To Already Exists Degree Award
     *
     * @return void
     */
    public function testUpdateToAlreadyExistsDegreeAward()
    {
        $degreeaward_db = DegreeAward::all()->random(1)->first();
        $degreeaward = factory(DegreeAward::class)->create();
         $response = $this->post('/update/degreeAward',['degreeaward_id'=>$degreeaward_db->DegreeAwardID,'degreeaward'=>$degreeaward->DegreeAward,'degreeawardcode'=>$degreeaward->DegreeAwardCode,'degreetype_id'=>$degreeaward->DegreeTypeID]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }


     /**
     * Delete Requires Degree Award ID
     *
     * @return void
     */
    public function testDeleteRequiresDegreeAwardID()
    {
        $response = $this->post('/delete/degreeAward',[]);
        $response->assertStatus(302)->assertSessionHasErrors('degreeaward_id');
    }


     /**
     * Delete Success Degree Award 
     *
     * @return void
     */
    public function testDeleteSuccessDegreeAward()
    {
        $degreeaward = factory(DegreeAward::class)->create();
        $response = $this->post('/delete/degreeAward',['degreeaward_id'=>$degreeaward->DegreeAwardID]);
        $response->assertStatus(302)->assertSessionHas('success');
    }

     /**
     * Get JSON Degree Award
     *
     * @return void
     */
    public function testGetJSONDegreeAward()
    {
        $degreeaward = DegreeAward::all()->random(1)->first();
        $response = $this->json('GET',"/api/degreeaward/{$degreeaward->DegreeAwardID}");
        $response->assertStatus(200)->assertJsonFragment(['data'=>$degreeaward->toArray()]);
    }


}
