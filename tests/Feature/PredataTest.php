<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\PredataCategory;
use App\PreData;
use Illuminate\Support\Facades\DB;

class PredataTest extends TestCase
{

   
  

    /**
     * test when no category is given
     *
     * @return void
     */
    public function testEmptyCategory()
    {
        
        $data = ['predata'=>'nocategorytest'];
       $response = $this->post('/save/predata',$data);
        $response->assertStatus(302)->assertSessionHasErrors(['cat_id']);
    }


    /**
     * test when no data is given
     *
     * @return void
     */
    public function testEmptyData()
    {
        $data = ['predata'=>'','cat_id'=>''];
        $response = $this->post('/save/predata',$data);
        $response->assertStatus(302)->assertSessionHasErrors(['cat_id','predata']);
    }

    /**
     * test when adding a name that already exists
     *
     * @return void
     */
    public function testAddingExistingData()
    {
        $data =  Predata::all()->first();
        $sending_data = ['data'=>$data->Data,'predata_id'=>$data->DataID,'cat_id'=>$data->CategoryID];
        $response = $this->post('/edit/predata',$sending_data);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }


    /**
     * test when valid data is given
     *
     * @return void
     */
    public function testSuccessData()
    {
    
        $data = factory(\App\Predata::class)->make();
        $sending_data = ['predata'=>$data->Data,'cat_id'=>$data->CategoryID];
        $response = $this->post('/save/predata',$sending_data);
        $response->assertStatus(302)->assertSessionHas('success','Predata successfully added');
       
    }


    /**
     * test when editing to a name that exists
     *
     * @return void
     */
    public function testEditToExistingName()
    {
        $predata1 = Predata::all()->random(1)->first();
        $predata2 = Predata::all()->random(1)->first();
        $sending_data = ['predata_id'=>$predata1->DataID,'data'=>$predata2->Data,'cat_id'=>$predata2->CategoryID];
        $response = $this->post('/edit/predata',$sending_data);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }


    /**
     * test valid delete request
     *
     * @return void
     */
    public function testDeleteData()
    {
        $predata = Predata::all()->random(1)->first();
        $response = $this->post('/delete/predata',['predata_id'=>$predata->DataID]);
        $response->assertStatus(302)->assertSessionHas('success','Predata was deleted');
    }

    /**
     * test valid invalid delete request
     *
     * @return void
     */
    public function testDeleteRequiresDataID()
    {
        $predata = Predata::all()->random(1)->first();
        $response = $this->post('/delete/predata',['predata_id'=>null]);
        $response->assertStatus(302)->assertSessionHasErrors(['predata_id']);
    }
}
