<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\LGA;

class LGATest extends TestCase
{
    /**
     * Create Requires State ID
     *
     * @return void
     */
    public function testCreateRequiresStateID()
    {
        $response = $this->post('/save/lga',['lga'=>'Khana']);
        $response->assertStatus(302)->assertSessionHasErrors(['state_id']);
    }

     /**
     * Create Success
     *
     * @return void
     */
    public function testCreateSuccess()
    {
        $lga = factory(LGA::class)->make();
        $response = $this->post('/save/lga',['lga'=>$lga->LGA,'state_id'=>$lga->StateID]);
        $response->assertStatus(302)->assertSessionHas(['success']);
    }

     /**
     * Create Already Exists
     *
     * @return void
     */
    public function testCreateAlreadyExistsLGA()
    {
        $lga = factory(LGA::class)->create();
        $response = $this->post('/save/lga',['lga'=>$lga->LGA,'state_id'=>$lga->StateID]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }

     /**
     * Update To Already Exists
     *
     * @return void
     */
    public function testEditToAlreadyExistsLGA()
    {
        $lga = factory(LGA::class)->create();
        $lga_db = LGA::all()->random(1)->first();
        $response = $this->post('/update/lga',['lga'=>$lga->LGA,'state_id'=>$lga->StateID,'lga_id'=>$lga_db->LGAID]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }

     /**
     * Delete Requires ID
     *
     * @return void
     */
    public function testDeleteRequiresID()
    {
      
        $response = $this->post('/delete/lga',[]);
        $response->assertStatus(302)->assertSessionHasErrors(['lga_id']);
    }

     /**
     * Delete Success
     *
     * @return void
     */
    // public function testDeleteSuccess()
    // {
    //     $lga = factory(LGA::class)->create();
    //     $response = $this->post('/delete/lga',['lga_id'=>$lga->LGAID]);
    //     $response->assertStatus(302)->assertSessionHas(['success']);
    // }


     /**
     * Get JSON  LGA
     *
     * @return void
     */
    public function testGetJSONLGA()
    {
        $lga = LGA::all()->random(1)->first();
        $response = $this->json('GET',"/api/lga/{$lga->LGAID}");
        $response->assertStatus(200)->assertJsonFragment(['data'=>$lga->toArray()]);
    }

    
}
