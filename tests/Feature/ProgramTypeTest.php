<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\ProgramType;

class ProgramTypeTest extends TestCase
{
    /**
     * Create Requires Code
     *
     * @return void
     */
    public function testCreateRequiresProgramTypeCode()
    {
        $response = $this->post('/save/programType',['programtype'=>'testprogram']);
        $response->assertStatus(302)->assertSessionHasErrors(['programtypecode']);
    }

      /**
     * Create Success
     *
     * @return void
     */
    public function testCreateSuccessProgramType()
    {
        $programtype = factory(ProgramType::class)->make();
        $response = $this->post('/save/programType',['programtype'=>$programtype->ProgramType,'programtypecode'=>$programtype->ProgramTypeCode]);
        $response->assertStatus(302)->assertSessionHas(['success']);
    }

      /**
     * Create Already Exists ProgramType
     *
     * @return void
     */
    public function testCreateAlreadyExistsProgramType()
    {
        $programtype = factory(ProgramType::class)->create();
        $response = $this->post('/save/programType',['programtype'=>$programtype->ProgramType,'programtypecode'=>$programtype->ProgramTypeCode]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }

      /**
     * Update Already Exists ProgramType
     *
     * @return void
     */
    public function testUpdateAlreadyExistsProgramType()
    {
        $programtype_db = ProgramType::all()->first();
        $programtype = factory(ProgramType::class)->create();
        $response = $this->post('/update/programType',['programtype'=>$programtype->ProgramType,'programtypecode'=>$programtype->ProgramTypeCode,'programtype_id'=>$programtype_db->ProgramTypeID]);
        $response->assertStatus(302)->assertSessionHas(['error']);
    }


      /**
     * Delete Requires ProgramTypeID
     *
     * @return void
     */
    public function testDeleteRequiresProgramTypeID()
    {
        $response = $this->post('/delete/programType',[]);
        $response->assertStatus(302)->assertSessionHasErrors('programtype_id');
    }

      /**
     * Delete Success
     *
     * @return void
     */
    public function testDeleteSuccess()
    {
        $programtype = factory(ProgramType::class)->create();
        $response = $this->post('/delete/programType',['programtype_id'=>$programtype->ProgramTypeID]);
        $response->assertStatus(302)->assertSessionHas('success');
    }


     /**
     * Get JSON Program Type
     *
     * @return void
     */
    public function testGetJSONStateLGA()
    {
        $programtype = ProgramType::all()->random(1)->first();
        $response = $this->json('GET',"/api/programtype/{$programtype->ProgramTypeID}");
        $response->assertStatus(200)->assertJsonFragment(['data'=>$programtype->toArray()]);
    }
}
