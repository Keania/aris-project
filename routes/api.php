<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/predata/category/{category}','AjaxResourceController@getCategoryPredatas');
Route::get('/predata/{predata}','AjaxResourceController@getPredata');

Route::get('facultycolour/{facultycolour}','AjaxResourceController@getColour');

Route::get('lga/state/{state}','AjaxResourceController@getStateLgas');
Route::get('lga/{lga}','AjaxResourceController@getLGA');
Route::get('state/{state}','AjaxResourceController@getState');
Route::get('feeitem/{feeitem}','AjaxResourceController@getFeeItem');

Route::get('/olevelsubject/{olevelsubject}','AjaxResourceController@getOlevelSubject');
Route::get('/degreetype/{degreetype}','AjaxResourceController@getDegreeType');
Route::get('/programtype/{programtype}','AjaxResourceController@getProgramType');
Route::get('/degreeaward/degreetype/{degreetype}','AjaxResourceController@getDegreeAwardForType');
Route::get('/degreeaward/{degreeaward}','AjaxResourceController@getDegreeAward');
