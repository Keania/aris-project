<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\DegreeType;
use Faker\Generator as Faker;

$factory->define(DegreeType::class, function (Faker $faker) {
    return [
        //
        'DegreeType'=>$faker->sentence(2)
    ];
});
