<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\ProgramType;
use Faker\Generator as Faker;

$factory->define(ProgramType::class, function (Faker $faker) {
    return [
        //
        'ProgramType'=>$faker->sentence(2),
        'ProgramTypeCode'=>$faker->countryCode
    ];
});
