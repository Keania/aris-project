@extends('app')
@section('page.title')
Degree Award Set Up
@endsection

@section('page.content')
<div class="section__content">

    <div id="degreeaward">
        <h3><i>Set up</i> Degree Award</h3>

        <div id="add__degreeAward" class="setup__divs">
            <h5>New Degree Award definition</h5>
            <form method="POST" action="{{route('save.degreeAward')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select Degree Type</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('degreetype_id') ? 'invalid' : ''}} " name="degreetype_id">

                            @if(count($degree_types) > 0)
                            <option value="null">--Select Degree Type--</option>
                            @foreach($degree_types as $degreetype)
                            <option value="{{$degreetype->DegreeTypeID}}">{{$degreetype->DegreeType}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                        @if($errors->has('degreetype_id'))
                        <span class="errors">{{$errors->first('degreetype_id')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Degree Award</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{$errors->has('degreeaward') ? 'invalid' : ''}}" name="degreeaward">
                        @if($errors->has('degreeaward'))
                        <span class="errors">{{$errors->first('degreeaward')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Degree Award Code</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{$errors->has('degreeawardcode') ? 'invalid' : ''}}" name="degreeawardcode">
                        @if($errors->has('degreeawardcode'))
                        <span class="errors">{{$errors->first('degreeawardcode')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Add" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#edit__degreeaward" value="Edit" class="btn btn-primary">
                    <input type="button" data-toggle="collapse" data-target="#delete__degreeaward" value="Delete" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="edit__degreeaward" class="collapse setup__divs">

            <h5>Degree Award Modification</h5>
            <form method="POST" action="{{route('update.degreeAward')}}">
                @csrf
                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select Degree Type</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('degreetype_id') ? 'invalid' : ''}} " name="degreetype_id" onchange="getDegreeAwardForType(event)">

                            @if(count($degree_types) > 0)
                            <option value="null">--Select Degree Type--</option>
                            @foreach($degree_types as $degreetype)
                            <option value="{{$degreetype->DegreeTypeID}}">{{$degreetype->DegreeType}}</option>
                            @endforeach
                            @else
                            <option>---- No data available --</option>
                            @endif
                        </select>
                        @if($errors->has('degreetype_id'))
                        <span class="errors">{{$errors->first('degreetype_id')}}</span>
                        @endif
                    </div>

                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Select Degree Award</label>
                    <div class="col-md-6">
                        <select class="form-control  {{$errors->has('degreeaward_id') ? 'invalid' : ''}} " id="degreeawards" name="degreeaward_id" onchange="getDegreeAward(event)">
                            <option>---- No data available --</option>
                        </select>
                        @if($errors->has('degreeaward_id'))
                        <span class="errors">{{$errors->first('degreeaward_id')}}</span>
                        @endif
                    </div>

                </div>



                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Degree Award</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control  {{$errors->has('degreeaward') ? 'invalid' : ''}} " name="degreeaward" id="editDegreeAwardInput">
                        @if($errors->has('degreeaward'))
                        <span class="errors">{{$errors->first('degreeaward')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6 row">
                    <label class="form-label col-md-4">Degree Award Code</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control  {{$errors->has('degreeawardcode') ? 'invalid' : ''}} " name="degreeawardcode" id="editDegreeAwardCodeInput">
                        @if($errors->has('degreeawardcode'))
                        <span class="errors">{{$errors->first('degreeawardcode')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Save" class="btn btn-primary">
                </div>
            </form>
        </div>

        <div id="delete__degreeaward" class="collapse setup__divs">
            <h5>Degree Award deletion</h5>
            <form method="POST" action="{{route('delete.degreeAward')}}">
                @csrf
                <div class="form-group col-md-6">
                    <label class="form-label">Select Degree award</label>
                    <select class="form-control" name="degreeaward_id">
                        @if(count($degree_awards) > 0)
                        @foreach($degree_awards as $degree_award)
                        <option value="{{$degree_award->DegreeAwardID}}">{{$degree_award->DegreeAward}}</option>
                        @endforeach
                        @else
                        <option>No data available</option>
                        @endif
                    </select>

                </div>

                <div class="form-group col-md-6">
                    <input type="submit" name="submit" value="Delete" class="btn btn-primary">

                </div>
            </form>
        </div>

        <div class="col-md-6">
            <table class="table">
                <thead>
                    <tr>
                        <th>Degree Award</th>
                        <th>Degree Award Code</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($degree_awards as $award)
                        <tr>
                            <td>{{$award->DegreeAward}}</td>
                            <td>{{$award->DegreeAwardCode}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection
@section('page.script')
<script>
    var getDegreeAwardForType = function(evt) {

        let select = evt.target.value;
        if (isNaN(select)) {
            return false;
        }
        $.getJSON('/api/degreeaward/degreetype/' + select, function(response) {}).done(function(response) {
            var selecter = document.getElementById('degreeawards');
            selecter.innerHTML = "";
            response.data.forEach(function(degreeaward) {
                console.log(degreeaward)
                option = document.createElement('option')
                option.setAttribute('value', degreeaward.DegreeAwardID);
                option.innerHTML = degreeaward.DegreeAward;
                selecter.appendChild(option);
            })

        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        })

    }

    var getDegreeAward = function(evt) {

        let select = evt.target.value;
        console.log(select)
        if (isNaN(select)) {
            return false;
        }
        $.getJSON('/api/degreeaward/' + select, function(response) {}).done(function(response) {
            document.getElementById('editDegreeAwardInput').value = response.data.DegreeAward;
            document.getElementById('editDegreeAwardCodeInput').value = response.data.DegreeAwardCode;
        }).fail(function(error) {
            $('#ajax-error').text('Error ' + error.statusText + ' for request ');
        })

    }
</script>
@endsection