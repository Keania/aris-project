<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreData extends Model
{
    //

    protected $guarded = [];
    protected $table = 'tblpredata';
    public $timestamps = false;


    public function getRouteKeyName()
    {
        return 'DataID';
    }

    public function getKeyName()
    {
        return 'DataID';
    }
}
