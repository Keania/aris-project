<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DegreeAward extends Model
{
    //
    protected $guarded = [];
    protected $table = 'tbldegreeaward';
    public $timestamps = false;


    public function getRouteKeyName()
    {
        return 'DegreeAwardID';
    }

    public function getKeyName()
    {
        return 'DegreeAwardID';
    }
}
